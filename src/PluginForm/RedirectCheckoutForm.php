<?php

namespace Drupal\commerce_powertranz\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_price\Entity\Currency;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class RedirectCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    // Payment data.
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $numeric_currency_code = Currency::load($currency_code)->getNumericCode();
    $amount = $payment->getAmount()->getNumber();
    $order_number = ''; // initialize to empty string for now
    $data['variables[payment_gateway]'] = $payment->getPaymentGatewayId();
    $data['variables[order]'] = $payment->getOrderId();

    // Order and billing address.
    $order = $payment->getOrder();
    $billing_address = $order->getBillingProfile()->get('address')->first();

    // If Order number is not yet set (which it typically isn't at this point),
    // then we will set it here so that we can use it as part of the Hosted Payment Page Authorization Request in
    // the onReturn() method in the PowerTranzOffsiteCheckout.
    if (!$order->getOrderNumber()) {
      $entity_type_manager = \Drupal::entityTypeManager();
      $order_type_storage = $entity_type_manager->getStorage('commerce_order_type');
      $order_type = $order_type_storage->load($order->bundle());
      /** @var \Drupal\commerce_number_pattern\Entity\NumberPatternInterface $number_pattern */
      $number_pattern = $order_type->getNumberPattern();
      if ($number_pattern) {
        $order_number = $number_pattern->getPlugin()->generate($order);
      }
      else {
        $order_number = $order->id();
      }

      $order->setOrderNumber($order_number);
    } else {
      $order_number = $order->getOrderNumber();
    }

    $order->save();

    $data = [
      "TransactionIdentifier" => \Drupal::service('uuid')->generate(),
      "TotalAmount" => $amount,
      "CurrencyCode" => $numeric_currency_code,
      "ThreeDSecure" => TRUE,
      "Source" => [
        "CardPan" => $order->getData('card_number'),
        "CardCvv" => '', //$order->getData('security_code'),
        "CardExpiration" => '', //substr($payment_method->card_exp_year->value, 2, 2) . str_pad($payment_method->card_exp_month->value, 2, '0', STR_PAD_LEFT),
        "CardholderName" => $billing_address->given_name . ' ' . $billing_address->family_name,
      ],
      "OrderIdentifier" => $order_number,
      "BillingAddress" => [
        "FirstName" =>  $billing_address->given_name,
        "LastName" =>  $billing_address->family_name,
        "Line1" => $billing_address->address_line1,
        "Line2" => $billing_address->address_line2,
        "City" => $billing_address->locality,
        "State" => $billing_address->administrative_area,
        "PostalCode" => $billing_address->postal_code,
        "CountryCode" => $billing_address->country_code, // @todo verify if numeric currency code is the same as numeric country code. Might be able to ignore this?
        "EmailAddress" => $order->getEmail(), //mail->getValue(),
        "PhoneNumber" => "876-366-3946",
      ],
      "AddressMatch" => FALSE,
      "ExtendedData" => [
        "ThreeDSecure" => [
          "ChallengeWindowSize" => 4,
          "ChallengeIndicator" => "01",
        ],
        "MerchantResponseURL" => Url::fromUri("internal:/checkout/{$payment->getOrderId()}/payment/return",
          ['absolute' => TRUE])->toString(),
      ],
    ];
    dump($data);
    die();
    // $url = $payment_gateway_plugin->getApiUrl() . 'HostedPagePreprocess';
    $url = "https://staging.ptranz.com/api/spi/Auth";
    $client = \Drupal::httpClient();
    $request = $client->request('POST', $url, [
      // 'verify' => false,
      // 'debug' => true,
      'json' => $data,
      'headers' => [
        'PowerTranz-PowerTranzId' => $configuration['powertranz_merchant_id'],
        'PowerTranz-PowerTranzPassword' => $configuration['powertranz_password']
      ]
    ]);
    $facResponse = $request->getBody()->getContents();

    $hostedPageAuthorizeResponse = json_decode($facResponse);

    $hostedPageUrl = $payment_gateway_plugin->getHostedPageUrl();

    if (isset($hostedPageAuthorizeResponse->ResponseCode) && $hostedPageAuthorizeResponse->ResponseCode == '0') {
      $redirectUrl = $hostedPageUrl . $hostedPageAuthorizeResponse->SecurityToken;
      // dpm($hostedPageAuthorizeResponse->SingleUseToken);
      // dpm($hostedPageAuthorizeResponse->ResponseCodeDescription);
    } else {
      $messenger = \Drupal::messenger();
      $messenger->addMessage(t('There was an error processing this request. Please check your information and try again'), $messenger::TYPE_ERROR);

      // Log the response
      \Drupal::logger('commerce_powertranz')
        ->error('There was an error processing this request. Reason Code: @reasonCode', [
          '@reasonCode' => var_export($hostedPageAuthorizeResponse->ReasonCode, TRUE),
        ]);
    }

    // Form url values.
    $data['continueurl'] = $form['#return_url'];
    $data['cancelurl'] = $form['#cancel_url'];

    return $this->buildRedirectForm($form, $form_state, $redirectUrl, $data, self::REDIRECT_POST);
  }

}
