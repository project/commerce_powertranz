<?php

namespace Drupal\commerce_powertranz\Plugin\Commerce\PaymentGateway;

use CommerceGuys\Addressing\Country\CountryRepository;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsCreatingPaymentMethodsInterface;
use Drupal\commerce_price\Entity\Currency;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the First Atlantic Commerce offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "powertranz_offsite_checkout",
 *   label = @Translation("PowerTranz (Offsite)"),
 *   display_label = @Translation("PowerTranz"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_powertranz\PluginForm\RedirectCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class PowerTranzOffsiteCheckout extends OffsitePaymentGatewayBase implements SupportsCreatingPaymentMethodsInterface {
  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Messenger
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.channel.commerce_powertranz');
    $instance->httpClient = $container->get('http_client');
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
        'powertranz_merchant_id' => '',
        'powertranz_acquirer_id' => '',
        'powertranz_password' => '',
        'powertranz_hpp_pageset' => '',
        'powertranz_hpp_pagename' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['powertranz_merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FAC Merchant ID'),
      '#description' => $this->t('Your FAC ID provided by FAC'),
      '#default_value' => $this->configuration['powertranz_merchant_id'],
      '#required' => TRUE,
    ];

    $form['powertranz_acquirer_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FAC Acquirer ID'),
      '#description' => $this->t('The Acquirer ID for your FAC Merchant Account.'),
      '#default_value' => $this->configuration['powertranz_acquirer_id'],
      '#required' => TRUE,
    ];

    $form['powertranz_password'] = [
      '#type' => 'password',
      '#title' => $this->t('FAC Password'),
      '#description' => $this->t('The password for your FAC Merchant Account.'),
      '#default_value' => $this->configuration['powertranz_password'],
      '#required' => TRUE,
    ];

    $form['powertranz_hpp_pageset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FAC Hosted Payment Page (HPP) Page Set'),
      '#description' => $this->t('The Name of a Set of Pages. This can be anything but should be your business division name. This gives assurance to Cardholders that they are dealing with Merchant even when paying directly to FAC. Note: Do not put any spaces in the name as it forms part of the URL for the Page.'),
      '#default_value' => $this->configuration['powertranz_hpp_pageset'],
      '#required' => FALSE,
    ];

    $form['powertranz_hpp_pagename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FAC Hosted Payment Page (HPP) Page Name'),
      '#description' => $this->t('The Name of the page. E.g.: PayPage, PayNow. Can be whatever you decide. Again, do not use any spaces, as it is part of the Page URL.'),
      '#default_value' => $this->configuration['powertranz_hpp_pagename'],
      '#required' => FALSE,
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['powertranz_merchant_id'] = $values['powertranz_merchant_id'];
    $this->configuration['powertranz_acquirer_id'] = $values['powertranz_acquirer_id'];
    $this->configuration['powertranz_password'] = $values['powertranz_password'];
    $this->configuration['powertranz_hpp_pageset'] = $values['powertranz_hpp_pageset'];
    $this->configuration['powertranz_hpp_pagename'] = $values['powertranz_hpp_pagename'];
  }

  /**
   * {@inheritdoc}
   */
  public function getApiUrl() {
    if ($this->getMode() == 'test') {
      return 'https://staging.ptranz.com/api/';
    }
    else {
      return 'https://gateway.ptranz.com/api/';
    }
  }

  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details)
  {
    // dump($payment_details);
    // dump($payment_method);

    $_SESSION['card_number'] = $payment_details['number'];
    $_SESSION['card_exp'] = substr($payment_details['expiration']['year'], 2, 2) . str_pad($payment_details['expiration']['month'], 2, '0', STR_PAD_LEFT);
    $_SESSION['security_code'] = $payment_details['security_code'];
    // exit;
    // Don't save the payment method as we don't want to save the users credit card number
    $payment_method->setReusable(FALSE);
  }

  public function createPayment(PaymentInterface $payment, $capture = true)
  {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    $amount = $payment->getAmount()->getNumber();
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $numeric_currency_code = Currency::load($currency_code)->getNumericCode();
    $order = $payment->getOrder();
    $order_id = $payment->getOrderId();
    $orderNumber = $order->getOrderNumber();
    $billing_address = $order->getBillingProfile()->get('address')->first();
    // dump($order);
    $country_list = \Drupal::getContainer()->get('country_manager')->getList();
    $country_name = $country_list[$billing_address->country_code]->render();
    $numeric_country_code = (new CountryRepository())->get($billing_address->country_code)->getNumericCode();

    $data = [
      "TransactionIdentifier" => \Drupal::service('uuid')->generate(),
      "TotalAmount" => $amount,
      "CurrencyCode" => $numeric_currency_code,
      "ThreeDSecure" => TRUE,
      "Source" => [
        "CardPan" => $_SESSION['card_number'], //$order->getData('card_number'),
        "CardCvv" => $_SESSION['security_code'], //$payment_method->security_code->value, //$order->getData('security_code'),
        "CardExpiration" => substr($payment_method->card_exp_year->value, 2, 2) . str_pad($payment_method->card_exp_month->value, 2, '0', STR_PAD_LEFT),
        "CardholderName" => $billing_address->given_name . ' ' . $billing_address->family_name,
      ],
      "OrderIdentifier" => $orderNumber,
      "BillingAddress" => [
        "FirstName" =>  $billing_address->given_name,
        "LastName" =>  $billing_address->family_name,
        "Line1" => $billing_address->address_line1,
        "Line2" => $billing_address->address_line2,
        "City" => $billing_address->locality,
        "State" => '01', //$billing_address->administrative_area, // @todo get numeric state or two letter state as it returns a full name for Jamaican parishes.
        "PostalCode" => $billing_address->postal_code,
        "CountryCode" => $numeric_country_code, //'388', //$billing_address->country_code, // @todo get numeric country code as Drupal Commerce returns two-letter code
        "EmailAddress" => $order->getEmail(),
        "PhoneNumber" => "8769999999",
      ],
      "AddressMatch" => FALSE,
      "ExtendedData" => [
        "ThreeDSecure" => [
          "ChallengeWindowSize" => 4,
          "ChallengeIndicator" => "01",
        ],
        "MerchantResponseURL" => Url::fromUri("internal:/checkout/{$payment->getOrderId()}/payment/return",
          ['absolute' => TRUE])->toString(),
      ],
    ];

    // dump($data);
    // exit;

    try {
      $url = "https://staging.ptranz.com/api/spi/Sale";

      $request = $this->httpClient->request('POST', $url, [
        // 'verify' => false,
        // 'debug' => true,
        'json' => $data,
        'headers' => [
          'PowerTranz-PowerTranzId' => $configuration['powertranz_merchant_id'],
          'PowerTranz-PowerTranzPassword' => $configuration['powertranz_password']
        ]
      ]);
      $facResponse = $request->getBody()->getContents();
    }
    catch (\Exception $e) {
      // $messenger = \Drupal::messenger();
      $this->messenger->addMessage($this->t('An error occurred and processing did not complete.'), $this->messenger::TYPE_ERROR);
      throw new PaymentGatewayException('Payment gateway error');
    }

    // $payment->setState('new');
    // $payment->save();

    $response = json_decode($facResponse);

    // Check if we get back the 3DS Form, if we do then display it.
    // Otherwise, proceed with regular non-3DS authorization.
    if (isset($response->RedirectData) && $response->RedirectData != "") {
      echo $response->RedirectData;
      exit;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $fac_response = json_decode($request->request->get('Response'));
    // dump($fac_response);
    // dump($order);
    // dump($fac_response->SpiToken);
    // exit;
    try {
      $url = "https://staging.ptranz.com/api/spi/Payment";

      $request = $this->httpClient->request('POST', $url, [
        // 'verify' => false,
        // 'debug' => true,
        // 'json' => $data,
        'body' => json_encode($fac_response->SpiToken),
        'headers' => [
          'PowerTranz-PowerTranzId' => $this->configuration['powertranz_merchant_id'],
          'PowerTranz-PowerTranzPassword' => $this->configuration['powertranz_password'],
          'Content-Type' => 'application/json'
        ]
      ]);
      $facResponse = json_decode($request->getBody()->getContents(), TRUE);
      // dump($facResponse);
      // exit;

      if ($facResponse['IsoResponseCode'] === '00') {
        $payment = $payment_storage->create([
          'state' => 'completed',
          'amount' => $order->getBalance(),
          'payment_gateway' => $this->parentEntity->id(),
          'order_id' => $order->id(),
          'remote_id' => $facResponse['RRN'],
          'authorized' => time()
        ]);
        $payment->save();

        // Clear the card number and security code from the order data
        // We don't want to store these details any longer than we need.
        unset($_SESSION['card_number']); //$order->setData('card_number', NULL);
        unset($_SESSION['security_code']); //$order->setData('security_code', NULL);
      } else {
        $this->logger->debug("Transaction failed with IsoResponseCode {$facResponse['IsoResponseCode']}");
      }
    }
    catch (\Exception $e) {
      $this->messenger->addMessage($this->t('An error has occurred and processing did not complete.'), $this->messenger::TYPE_ERROR);
      throw new PaymentGatewayException($e->getMessage());
    }
  }

  public function deletePaymentMethod(PaymentMethodInterface $payment_method)
  {
    // TODO: Implement deletePaymentMethod() method.
  }
}
