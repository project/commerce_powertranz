<?php

namespace Drupal\commerce_powertranz\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Entity\Currency;
use Drupal\commerce_price\Repository\CurrencyRepository;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation;

/**
 * Provides the First Atlantic Commerce offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "powertranz_onsite_checkout",
 *   label = @Translation("PowerTranz (Onsite)"),
 *   display_label = @Translation("PowerTranz"),
 *    forms = {
 *     "add-payment-method" = "Drupal\commerce_powertranz\PluginForm\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class PowerTranzOnsiteCheckout extends OnsitePaymentGatewayBase implements SupportsAuthorizationsInterface, SupportsRefundsInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('http_client');
    $instance->logger = $container->get('logger.channel.commerce_powertranz');
    return $instance;
  }


  public function defaultConfiguration() {
    return [
        'powertranz_merchant_id' => '',
        'powertranz_acquirer_id' => '',
        'powertranz_password' => '',
        'powertranz_use3ds' => '',
      ] + parent::defaultConfiguration();
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['powertranz_merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FAC Merchant ID'),
      '#description' => $this->t('Your FAC ID provided by FAC'),
      '#default_value' => $this->configuration['powertranz_merchant_id'],
      '#required' => TRUE,
    ];

    $form['powertranz_acquirer_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FAC Acquirer ID'),
      '#description' => $this->t('The Acquirer ID for your FAC Merchant Account.'),
      '#default_value' => $this->configuration['powertranz_acquirer_id'],
      '#required' => TRUE,
    ];

    $form['powertranz_password'] = [
      '#type' => 'password',
      '#title' => $this->t('FAC Password'),
      '#description' => $this->t('The password for your FAC Merchant Account.'),
      '#default_value' => $this->configuration['powertranz_password'],
      '#required' => TRUE,
    ];

    $form['powertranz_use3ds'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use 3D-Secure'),
      '#description' => $this->t('Whether or not you would like to use 3D-Secure for processing transactions.'),
      '#default_value' => $this->configuration['powertranz_use3ds'],
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['powertranz_merchant_id'] = $values['powertranz_merchant_id'];
    $this->configuration['powertranz_acquirer_id'] = $values['powertranz_acquirer_id'];
    $this->configuration['powertranz_password'] = $values['powertranz_password'];
    $this->configuration['powertranz_use3ds'] = $values['powertranz_use3ds'];
  }

  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    $amount = $payment->getAmount()->getNumber();
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $numeric_currency_code = Currency::load($currency_code)->getNumericCode();
    $order = $payment->getOrder();
    $order_id = $payment->getOrderId();
    $orderNumber = $order->getOrderNumber();
    $billing_address = $order->getBillingProfile()->get('address')->first();

    $country_list = \Drupal::getContainer()->get('country_manager')->getList();
    $country_name = $country_list[$billing_address->country_code]->render();

    $data = [
      "TransactionIdentifier" => \Drupal::service('uuid')->generate(),
      "TotalAmount" => $amount,
      "CurrencyCode" => $numeric_currency_code,
      "ThreeDSecure" => TRUE,
      "Source" => [
        "CardPan" => $order->getData('card_number'),
        "CardCvv" => $order->getData('security_code'),
        "CardExpiration" => substr($payment_method->card_exp_year->value, 2, 2) . str_pad($payment_method->card_exp_month->value, 2, '0', STR_PAD_LEFT),
        "CardholderName" => $billing_address->given_name . ' ' . $billing_address->family_name,
      ],
      "OrderIdentifier" => 'TEST' . time(), //$orderNumber,
      "BillingAddress" => [
        "FirstName" =>  $billing_address->given_name,
        "LastName" =>  $billing_address->family_name,
        "Line1" => $billing_address->address_line1,
        "Line2" => $billing_address->address_line2,
        "City" => $billing_address->locality,
        "State" => $billing_address->administrative_area,
        "PostalCode" => $billing_address->postal_code,
        "CountryCode" => $billing_address->country_code, // @todo verify if numeric currency code is the same as numeric country code. Might be able to ignore this?
        "EmailAddress" => $order->getEmail(),
        "PhoneNumber" => "876-366-3946",
      ],
      "AddressMatch" => FALSE,
      "ExtendedData" => [
        "ThreeDSecure" => [
          "ChallengeWindowSize" => 4,
          "ChallengeIndicator" => "01",
        ],
        "MerchantResponseURL" => Url::fromUri("internal:/checkout/{$payment->getOrderId()}/payment/return",
          ['absolute' => TRUE])->toString(),
      ],
    ];

    // Clear the card number and security code from the order data
    // We don't want to store these details any longer than we need.
    $order->setData('card_number', NULL);
    $order->setData('security_code', NULL);

    try {
      $url = $this->getApiUrl() . "spi/Auth";
      $client = \Drupal::httpClient();
      $request = $client->request('POST', $url, [
        // 'verify' => false,
        // 'debug' => true,
        'json' => $data,
        'headers' => [
          'PowerTranz-PowerTranzId' => $configuration['powertranz_merchant_id'],
          'PowerTranz-PowerTranzPassword' => $configuration['powertranz_password']
        ]
      ]);
      $facResponse = $request->getBody()->getContents();
    }
    catch (\PaymentGatewayAPI\Exception $e) {
      $messenger = \Drupal::messenger();
      $messenger->addMessage($this->t('An error occurred and processing did not complete.'), $messenger::TYPE_ERROR);
      throw new PaymentGatewayException('Payment gateway error');
    }

    $payment->setState('new');
    $payment->save();

    $response = json_decode($facResponse);

    // Check if we get back the 3DS Form, if we do then display it.
    // Otherwise, proceed with regular non-3DS authorization.
    if (isset($response->RedirectData) && $response->RedirectData != "") {
      echo $response->RedirectData;
      exit;
    } else  {
      $messenger = \Drupal::messenger();
      $messenger->addMessage($this->t('There was an error processing this request. Reason Code:' . $response->IsoResponseCode), $messenger::TYPE_ERROR);
      // @todo Log the reason code and description
      $this->logger->error("There was an error processing {$orderNumber}. Reason Code was {$response->IsoResponseCode}.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    // Get Order Number
    $order = $payment->getOrder();
    $orderNumber = $order->getOrderNumber();

    $data = [
      "TransactionIdentifier" => '',
      "TotalAmount" => $amount,
    ];

    try {
      $url = "https://staging.ptranz.com/api/Capture";

      $request = $this->httpClient->request('POST', $url, [
        // 'verify' => false,
        // 'debug' => true,
        'json' => $data,
        'headers' => [
          'PowerTranz-PowerTranzId' => $configuration['powertranz_merchant_id'],
          'PowerTranz-PowerTranzPassword' => $configuration['powertranz_password']
        ]
      ]);
      $facResponse = $request->getBody()->getContents();
    }
    catch (\PaymentGatewayAPI\Exception $e) {
      $messenger = \Drupal::messenger();
      $messenger->addMessage($this->t('Capture failed.'), $messenger::TYPE_ERROR);
      throw new PaymentGatewayException('Payment gateway error');
    }

    if (isset($facResponse->IsoResponseCode) && $facResponse->IsoResponseCode == '00') {
      $payment->setState('completed');
      $payment->setRemoteId($facResponse->IsoResponseCode->RRN);
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    $data = [
      "TransactionIdentifier" => '',
      "TerminalCode" => '',
      "TerminalSerialNumber" => '',
      "AutoReversal" => false
    ];

    try {
      $url = $this->getApiUrl() . "void";

      $request = $this->httpClient->request('POST', $url, [
        // 'verify' => false,
        // 'debug' => true,
        'json' => $data,
        'headers' => [
          'PowerTranz-PowerTranzId' => $configuration['powertranz_merchant_id'],
          'PowerTranz-PowerTranzPassword' => $configuration['powertranz_password']
        ]
      ]);
      $facResponse = $request->getBody()->getContents();
    }
    catch (\PaymentGatewayAPI\Exception $e) {
      $messenger = \Drupal::messenger();
      $messenger->addMessage($this->t('Void failed.'), $messenger::TYPE_ERROR);
      throw new PaymentGatewayException('Payment gateway error');
    }

    if (isset($facResponse->IsoResponseCode) && $facResponse->IsoResponseCode == '00') {
      $payment->setState('authorization_voided');
      $payment->setRemoteId($facResponse->IsoResponseCode->RRN);
      $payment->save();
    } else  {
      $messenger = \Drupal::messenger();
      $messenger->addMessage(t('There was an error processing this request. Reason Code:' . $facResponse->IsoResponseCode), $messenger::TYPE_ERROR);
    }
  }


  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    // If not specified, refund the entire amount.
    // $amount = $amount ?: $payment->getAmount();
    $amount = $payment->getAmount()->getNumber();
    $this->assertRefundAmount($payment, $amount);
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $numeric_currency_code = Currency::load($currency_code)->getNumericCode();

    $data = [
      "Refund" => true,
      "TransactionIdentifier" => '',
      "TotalAmount" => $amount,
      "CurrencyCode" => $currency_code,
      "Source" => [
        "CardPresent" => false,
        "CardEmvFallback" => false,
        "ManualEntry" => false,
        "Debit" => false,
        "Contactless" => false,
        "CardPan" => "",
        "MaskedPan" => ""
      ],
      "TerminalCode" => '',
      "TerminalSerialNumber" => '',
      "AutoReversal" => false
    ];

    try {
      $url = $this->getApiUrl() . "refund";

      $request = $this->httpClient->request('POST', $url, [
        // 'verify' => false,
        // 'debug' => true,
        'json' => $data,
        'headers' => [
          'PowerTranz-PowerTranzId' => $configuration['powertranz_merchant_id'],
          'PowerTranz-PowerTranzPassword' => $configuration['powertranz_password']
        ]
      ]);
      $facResponse = $request->getBody()->getContents();
    }
    catch (\PaymentGatewayAPI\Exception $e) {
      $messenger = \Drupal::messenger();
      $messenger->addMessage($this->t('Refund failed.'), $messenger::TYPE_ERROR);
      throw new PaymentGatewayException('Payment gateway error');
    }

    if (isset($facResponse->IsoResponseCode) && $facResponse->IsoResponseCode == '00') {
      $payment->setState('refunded');
      $payment->setRemoteId($payment->getRemoteId());
      $payment->save();
    } else  {
      $messenger = \Drupal::messenger();
      $messenger->addMessage($this->t('There was an error processing this request. Reason Code:' . $facResponse->IsoResponseCode), $messenger::TYPE_ERROR);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $payment_method_type = $payment_method->getType()->getPluginId();
    // dpm($payment_details);

    // Don't save the payment method as we don't want to save the users credit card number
    $payment_method->setReusable(FALSE);
    // $expires = time() + 60;
    // $payment_method->setExpiresTime($expires);
    // $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // @todo: Implement this

    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function getApiUrl() {
    if ($this->getMode() == 'test') {
      return 'https://staging.ptranz.com/api/';
    }
    else {
      return 'https://gateway.ptranz.com/api/';
    }
  }

  /**
   * Processes the "return" request for 3-D Secure check.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function on3dsReturn(OrderInterface $order, $request, $capture = TRUE) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getPayment($order);

    $payment->setState('authorization');
    $payment->setRemoteId($request->request->get('ReferenceNo'));
    $payment->save();

    if ($capture) {
      $this->capturePayment($payment);
    }

    $payment_state = $capture ? 'completed' : 'authorization';
    $payment->setState($payment_state);
    $payment->setRemoteId($request->request->get('ReferenceNo'));
    $payment->save();
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @return bool|\Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected function getPayment(OrderInterface $order) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface[] $payments */
    $payments = $this->entityTypeManager
      ->getStorage('commerce_payment')
      ->loadByProperties(['order_id' => $order->id()]);

    if (empty($payments)) {
      return FALSE;
    }
    foreach ($payments as $payment) {
      if ($payment->getPaymentGateway()->getPluginId() !== 'powertranz_onsite_checkout' || $payment->getAmount()->compareTo($order->getTotalPrice()) !== 0) {
        continue;
      }
      $fac_payment = $payment;
    }
    return empty($fac_payment) ? FALSE : $fac_payment;
  }
}
